#include <stdio.h>
#include <stdlib.h>
#define BUFFERSIZE 64
#define NEGATIVE -1
#define POSITIVE 1
#define ENDMARK 0


int addCharNum(int original, char toAdd) {
	return(original*10 + (toAdd - '0'));
}

int isNum(char toTest) {
	if(toTest <= '9' && toTest >= '0') {
		return 1;
	}
	else 
		return 0;
}

int checkBuffer(int* buffer, int bufferSize, int toFind) {
	//check if the exponantial is in the buffer
	//Returns the index of the number to find in the buffer
	//-1 if not found.
	for(int n = 0; n < bufferSize; n++) {
		if(*(buffer+n) == toFind)
			return n;
	}
	return -1;
}

void sortBuffers(int* exp, int* base, int size) {
	if (size < 2)
		return;
	int pivot, first, last;
	pivot = exp[size / 2];
	for(first = 0, last = size - 1;; first++, last--){
		while(exp[first] < pivot)
			first++;
		while(exp[last] > pivot)
			last--;
		if(first >= last) break;
		int tmp = exp[first];
		exp[first] = exp[last];
		exp[last] = tmp;
		tmp = base[first];
		base[first] = base[last];
		base[last] = tmp;
	}
	sortBuffers(exp, base, first);
	sortBuffers(exp + first, base + first, size - first);
}

void printBuffers(int* exp, int* base, int size) {
	for(int n = 0; n < size; n++) {
		if(*(base + n) != 0)
			printf("%i,%i;", *(exp + n), *(base + n));
	}
	printf("\n;\n");
}

int main() {
	char nextChar;
	short flag = 1;
	short sign = POSITIVE;
	int* expo = (int*) calloc(BUFFERSIZE, sizeof(int));
	int* base = (int*) calloc(BUFFERSIZE, sizeof(int));
	int index, capacity;
	capacity = BUFFERSIZE;
	int fullNum = 0;
	int currentBufferSize = 0;
	while((nextChar=getchar()) != EOF) {
		if(isNum(nextChar)) {
			fullNum = addCharNum(fullNum, nextChar);
		}
			
		else if(nextChar == ';') {
			if(flag = ENDMARK) break;
			flag = ENDMARK; 
			base[index] += fullNum * sign;
			fullNum = 0;
			sign = POSITIVE;
		}
		
		else if(nextChar == ',') {
			index = checkBuffer(expo, currentBufferSize, fullNum * sign);
			if(index == -1) {
				if((currentBufferSize + 1) == capacity) {
					capacity *= 2;
					expo = (int*)realloc(expo, (capacity) * sizeof(int));
					base = (int*)realloc(base, (capacity) * sizeof(int));
				}
				expo[currentBufferSize] = sign * fullNum;
				index =	currentBufferSize++;
			}
			fullNum = 0;
			sign = POSITIVE;
		}
		else if(nextChar == '-') {
			sign = NEGATIVE;
		}

	}
	sortBuffers(expo, base, currentBufferSize);
	printBuffers(expo, base, currentBufferSize);
}
