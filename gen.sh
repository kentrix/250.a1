#!/bin/bash

LINES=2000000
trap SIGHUP 
i=0
if [ -e file ]; then
	rm file
fi

while [[ $i -le $LINES ]]; do
	let x=$RANDOM/100
	if [[ $i%2 -eq 1 ]]; then
		echo "$x;" >> file
	else 
		echo "$x," >> file
	fi
	let i=i+1
done
